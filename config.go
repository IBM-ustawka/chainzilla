package chainzilla

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type NetConfig struct {
	ID            int    `yaml:"id"`
	IPV4Addr      string `yaml:"ipv4addr"`
	Port          int    `yaml:"port"`
	WalletAddrHex string `yaml:"walletAddrHex"`
}

// StartupConfig is a struct that holds the startup configuration of the chainzilla engine instance.
type StartupConfig struct {
	Mnemonic           string
	AdjudicatorAddrHex string      `yaml:"adjudicatorAddrHex"` // Address of the adjudicator smart contract in the chain.
	AssetholderAddrHex string      `yaml:"assetholderAddrHex"` // Address of the assetholder smart contract in the chain.
	ChainURL           string      `yaml:"chainUrl"`           // Address of the ethereum blockchain
	IsBroker           bool        `yaml:"isBroker"`
	BrokerConfig       NetConfig   `yaml:"brokerConfig"`  // Broker's network and wallet config data
	ClientConfigs      []NetConfig `yaml:"clientConfigs"` // Client's network and wallet config data (if user is a client then populate with one entry)
}

// NewStartupConfig creates and populates a new chainzilla startup configuration instance with the
// use of a configuration file provided by the file variable.
func NewStartupConfig(file string) (sf StartupConfig, err error) {
	var bytes []byte
	bytes, err = ioutil.ReadFile(file)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(bytes, &sf)
	if err != nil {
		return
	}
	return
}
