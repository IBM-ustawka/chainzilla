package chainzilla

import (
	"fmt"
	"os/exec"
	"testing"
	"time"
)

func runGanache(mnemonic string, name string, port, blocktime, accounts int, t *testing.T) {
	cmd := exec.Command("docker", "run", "--rm", "-d",
		fmt.Sprintf("--publish=%d:8545", port),
		fmt.Sprintf("--name=%s", name),
		"trufflesuite/ganache-cli:latest",
		fmt.Sprintf("-b=%d", blocktime),
		fmt.Sprintf("-a=%d", accounts),
		fmt.Sprintf("-m=\"%s\"", mnemonic),
	)
	if err := cmd.Start(); err != nil {
		t.Log(err)
		t.Fail()
	}
}

func closeGanache(name string, t *testing.T) {
	r := recover()
	exec.Command("docker", "stop", name).Start()

	if r != nil {
		t.Log(r)
		t.Fail()
	}
}

// TestSingleTransaction tests a scenario between a broker and a client where client transfers
// some funds to the broker.
func TestSingleTransaction(t *testing.T) {
	// docker run --rm --publish=8545:8545 --name=chainzilla-ganache trufflesuite/ganache-cli:latest -a=2 -m="pistol kiwi shrug future ozone ostrich match remove crucial oblige cream critic"
	mnemonic := "pistol kiwi shrug future ozone ostrich match remove crucial oblige cream critic"

	// Setup a broker and a client.
	clientCfg := StartupConfig{
		Mnemonic:           mnemonic,
		AdjudicatorAddrHex: "0x079557d7549d7D44F4b00b51d2C532674129ed51",
		AssetholderAddrHex: "0x923439be515b6A928cB9650d70000a9044e49E85",
		ChainURL:           "ws://127.0.0.1:8545",
		IsBroker:           false,
		BrokerConfig: NetConfig{
			ID:            0,
			IPV4Addr:      "127.0.0.1",
			Port:          8401,
			WalletAddrHex: "0x2EE1ac154435f542ECEc55C5b0367650d8A5343B",
		},
		ClientConfigs: []NetConfig{NetConfig{
			ID:            1,
			IPV4Addr:      "127.0.0.1",
			Port:          8402,
			WalletAddrHex: "0x70765701b79a4e973dAbb4b30A72f5a845f22F9E",
		}},
	}
	brokerCfg := clientCfg
	brokerCfg.IsBroker = true

	broker, client := NewNode(RoleBroker, &brokerCfg), NewNode(RoleClient, &clientCfg)

	// Run a simple transaction via the protocol: Client Opens, Updates and Closes.
	if err := client.OpenChannel(clientCfg.BrokerConfig.ID); err != nil {
		panic(fmt.Errorf("opening channel: %w", err))
	}
	time.Sleep(time.Second / 4)
	if err := client.UpdateChannel(); err != nil {
		panic(fmt.Errorf("updating channel: %w", err))
	}
	if err := client.CloseChannel(); err != nil {
		panic(fmt.Errorf("closing channel: %w", err))
	}

	// Wait for both nodes to stop.
	t.Log("Waiting for the broker")
	<-broker.done
	t.Log("Waiting for the client")
	<-client.done
}

// TestMultipleClientsInARow tests a scenario between a broker and 4 clients where clients transfer
// some funds to the broker.
func TestMultipleClientsInARow(t *testing.T) {
	// docker run --rm --publish=8545:8545 --name=chainzilla-ganache trufflesuite/ganache-cli:latest -a=5 -m="pistol kiwi shrug future ozone ostrich match remove crucial oblige cream critic"

	// Run a docker instance of ganache for this test (and close it at the end of it)
	mnemonic := "pistol kiwi shrug future ozone ostrich match remove crucial oblige cream critic"

	addjAddrHex := "0x079557d7549d7D44F4b00b51d2C532674129ed51"
	assetHolderAddrHex := "0x923439be515b6A928cB9650d70000a9044e49E85"
	chainURL := "ws://127.0.0.1:8545"
	walletAddrs := []string{
		"0x2EE1ac154435f542ECEc55C5b0367650d8A5343B", // Broker's Wallet
		"0x70765701b79a4e973dAbb4b30A72f5a845f22F9E", // First client's wallet
		"0xF73C1cdA5bD32E6693D3cB313Bd9B9338d96f184", // Second client's wallet... etc.
		"0xC1b2444C0aAD4DE3f1E76C2F617B593eba75cb5a",
		"0x12A6C524c720759d275236a5725134Eb1c4749dD",
	}

	// Setup the broker network config
	brokerNetCfg := NetConfig{
		ID:            0,
		IPV4Addr:      "127.0.0.1",
		Port:          8401,
		WalletAddrHex: walletAddrs[0],
	}

	// Set up the client nodes
	clients := make([]*Node, 4)
	clientNetCfgs := make([]NetConfig, 4)
	clientStartupCfgs := make([]StartupConfig, 4)
	for i := 0; i < 4; i++ {
		clientNetCfg := NetConfig{
			ID:            i + 1,
			IPV4Addr:      "127.0.0.1",
			Port:          8402 + i,
			WalletAddrHex: walletAddrs[i+1],
		}
		cfg := StartupConfig{
			Mnemonic:           mnemonic,
			AdjudicatorAddrHex: addjAddrHex,
			AssetholderAddrHex: assetHolderAddrHex,
			ChainURL:           chainURL,
			IsBroker:           false,
			BrokerConfig:       brokerNetCfg,
			ClientConfigs:      []NetConfig{clientNetCfg},
		}
		// clients[i] = NewNode(RoleClient, &cfg)
		clientStartupCfgs[i] = cfg
		clientNetCfgs[i] = clientNetCfg
	}

	// Create a broker node
	brokerCfg := StartupConfig{
		Mnemonic:           mnemonic,
		AdjudicatorAddrHex: addjAddrHex,
		AssetholderAddrHex: assetHolderAddrHex,
		ChainURL:           chainURL,
		IsBroker:           true,
		BrokerConfig:       brokerNetCfg,
		ClientConfigs:      clientNetCfgs,
	}
	broker := NewNode(RoleBroker, &brokerCfg)

	// Set up clients
	for i := 0; i < 4; i++ {
		clients[i] = NewNode(RoleClient, &clientStartupCfgs[i])
	}

	// Run a simple transaction for each client via the protocol.
	for i := 0; i < 4; i++ {
		if err := clients[i].OpenChannel(broker.systemID); err != nil {
			panic(fmt.Errorf("opening channel: %w", err))
		}
		time.Sleep(time.Second / 4)
		if err := clients[i].UpdateChannel(); err != nil {
			panic(fmt.Errorf("updating channel: %w", err))
		}

		if err := clients[i].CloseChannel(); err != nil {
			panic(fmt.Errorf("closing channel: %w", err))
		}

		// Wait for all the nodes to stop.
		t.Logf("[client %d] Waiting for the broker", i)
		<-broker.done
		t.Logf("[client %d] Waiting for the client", i)
		<-clients[i].done
		time.Sleep(time.Second / 4)
	}
}
