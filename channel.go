package chainzilla

import (
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"

	ethchannel "perun.network/go-perun/backend/ethereum/channel"
	ethwallet "perun.network/go-perun/backend/ethereum/wallet"
	"perun.network/go-perun/channel"
	"perun.network/go-perun/wallet"
	"perun.network/go-perun/wire/net"
	"perun.network/go-perun/wire/net/simple"
)

// setupFunder sets up a perun channel funder struct.
func setupFunder(contractBackend ethchannel.ContractBackend, account accounts.Account, assetHolder common.Address) channel.Funder {
	ethDepositor := new(ethchannel.ETHDepositor)
	accounts := map[ethchannel.Asset]accounts.Account{ethwallet.Address(assetHolder): account}
	depositors := map[ethchannel.Asset]ethchannel.Depositor{ethwallet.Address(assetHolder): ethDepositor}
	return ethchannel.NewFunder(contractBackend, accounts, depositors)
}

// setupNetwork sets up a perun network bus and returns it along with a net.Listener.
func setupNetwork(myID int, account wallet.Account, rHosts map[int]string, rAddrs map[int]*ethwallet.Address) (listener net.Listener, bus *net.Bus, err error) {
	dialer := simple.NewTCPDialer(10 * time.Second)
	for key, host := range rHosts {
		if key != myID {
			dialer.Register(rAddrs[key], host)
		}
	}

	listener, err = simple.NewTCPListener(rHosts[myID])
	if err != nil {
		err = fmt.Errorf("creating listener: %w", err)
		return
	}

	bus = net.NewBus(account, dialer)
	return
}
