module gitlab.com/IBM-ustawka/chainzilla

go 1.16

require (
	github.com/ethereum/go-ethereum v1.10.2
	github.com/miguelmota/go-ethereum-hdwallet v0.0.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
	perun.network/go-perun v0.6.0
)
