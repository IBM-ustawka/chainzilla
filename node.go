package chainzilla

import (
	"context"
	"fmt"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/common"
	ethchannel "perun.network/go-perun/backend/ethereum/channel"
	ethwallet "perun.network/go-perun/backend/ethereum/wallet"
	"perun.network/go-perun/backend/ethereum/wallet/hd"
	"perun.network/go-perun/channel"
	"perun.network/go-perun/client"
	"perun.network/go-perun/wallet"
	"perun.network/go-perun/wire"
	"perun.network/go-perun/wire/net"
)

// NodeRole defines what role a node will fullfil in a chainzilla powered system.
type NodeRole int

const (
	RoleBroker NodeRole = iota
	RoleClient
)

// node represents a chainzilla engine node used which is used for off-chain currency exchange.
type Node struct {
	role   NodeRole
	config *StartupConfig

	hostnames map[int]string             // hostnames of the hosts partaking in a chainzilla network. Key is the id of the account in the database.
	addrs     map[int]*ethwallet.Address // addresses of the wallets in a chainzilla network. Key is the id of the account in the database.
	systemID  int                        // ID of the account using this node in the database.

	account         wallet.Account
	transactor      *hd.Transactor
	contractBackend ethchannel.ContractBackend
	assetholder     common.Address
	listener        net.Listener
	bus             *net.Bus
	client          *client.Client
	ch              *client.Channel
	done            chan struct{} // Signals that the channel got closed.
}

// NewNode is a chainzilla node constructor.
func NewNode(role NodeRole, confPtr *StartupConfig) (nodeptr *Node) {
	nodeInst := &Node{}
	config := *confPtr

	if config.IsBroker {
		nodeInst.systemID = config.BrokerConfig.ID
	} else {
		nodeInst.systemID = config.ClientConfigs[0].ID
	}

	// Configure host names
	nodeInst.hostnames = make(map[int]string)
	nodeInst.hostnames[config.BrokerConfig.ID] = fmt.Sprintf("%s:%d",
		config.BrokerConfig.IPV4Addr,
		config.BrokerConfig.Port)
	for _, cfg := range config.ClientConfigs {
		nodeInst.hostnames[cfg.ID] = fmt.Sprintf("%s:%d", cfg.IPV4Addr, cfg.Port)
	}

	// Configure wallet addresses
	nodeInst.addrs = make(map[int]*ethwallet.Address)
	nodeInst.addrs[config.BrokerConfig.ID] = ethwallet.AsWalletAddr(common.HexToAddress(config.BrokerConfig.WalletAddrHex))
	for _, cfg := range config.ClientConfigs {
		nodeInst.addrs[cfg.ID] = ethwallet.AsWalletAddr(common.HexToAddress(cfg.WalletAddrHex))
	}

	// Set up the wallet for this node.
	account, wallet, err := setupWallet(role, config.Mnemonic)
	if err != nil {
		panic(fmt.Sprintf("setting up wallet: %v", err))
	}

	// Set up a transactor for the chain of id 1337 (id which is used by ganache)
	transactor := createTransactor(wallet)

	// Connect to the chain and retrieve the contract backend (which is used for either deployment
	// or validation of smart contracts)
	_, contractBackend, err := connectToChain(transactor, config.ChainURL)
	if err != nil {
		panic(fmt.Sprintf("connecting to chain: %v", err))
	}

	// Set up contracts (deploys or validates contracts based on given role)
	// (This tends to timeout when debugging so unless necessary skip it quickly)
	adjudicator, assetholder, err := setupContracts(role, contractBackend, account.Account,
		config.AdjudicatorAddrHex, config.AssetholderAddrHex)
	if err != nil {
		panic(fmt.Errorf("setting up contracts: %w", err))
	}

	// Create a new bus for the underlying perun network and retrieve the tcp listener.
	// listener, bus, err := setupNetwork(role, account, nodeInst.hostnames, nodeInst.addrs)
	listener, bus, err := setupNetwork(nodeInst.systemID, account, nodeInst.hostnames, nodeInst.addrs)
	if err != nil {
		panic(fmt.Errorf("setting up network: %w", err))
	}

	// Create an ethereum funder instance and a perun client based on that
	funder := setupFunder(contractBackend, account.Account, assetholder)
	// cl, err := client.New(nodeInst.addrs[role], bus, funder, adjudicator, wallet)
	cl, err := client.New(nodeInst.addrs[nodeInst.systemID], bus, funder, adjudicator, wallet)
	if err != nil {
		panic(fmt.Errorf("creating client: %w", err))
	}

	// Create the node that defines all event handlers for go-perun.
	nodeInst.role = role
	nodeInst.config = confPtr
	nodeInst.account = account
	nodeInst.transactor = transactor
	nodeInst.contractBackend = contractBackend
	nodeInst.assetholder = assetholder
	nodeInst.listener = listener
	nodeInst.bus = bus
	nodeInst.client = cl
	nodeInst.ch = nil
	nodeInst.done = make(chan struct{})

	// Set the NewChannel handler.
	cl.OnNewChannel(nodeInst.HandleNewChannel)

	// Start Proposal- and UpdateHandlers.
	go cl.Handle(nodeInst, nodeInst)

	// Listen on incoming connections.
	go bus.Listen(listener)

	return nodeInst
}

func (n *Node) OpenChannel(targetID int) error {
	if _, ok := n.addrs[targetID]; !ok {
		return fmt.Errorf("tried to open a channel with an unregistered channel %d", targetID)
	}

	fmt.Printf("Opening channel from %v to %v\n", n.systemID, targetID)
	// Alice and Bob will both start with 5 ETH.
	initBal := ethToWei(5)
	// Perun needs an initial allocation which defines the balances of all
	// participants. The same structure is used for multi-asset channels.
	initBals := &channel.Allocation{
		Assets:   []channel.Asset{ethwallet.AsWalletAddr(n.assetholder)},
		Balances: [][]*big.Int{{initBal, initBal}},
	}
	// All perun identities that we want to open a channel with. In this case
	// we use the same on- and off-chain accounts but you could use different.
	peers := []wire.Address{
		n.addrs[n.systemID],
		n.addrs[targetID],
	}

	// Prepare the proposal by defining the channel parameters.
	// TODO Ethereum account state validation before proposal
	proposal, err := client.NewLedgerChannelProposal(10, n.addrs[n.systemID], initBals, peers)
	if err != nil {
		return fmt.Errorf("creating channel proposal: %w", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Send the proposal.
	channel, err := n.client.ProposeChannel(ctx, proposal)
	if err != nil {
		return fmt.Errorf("proposing channel: %w", err)
	}
	fmt.Printf("🎉 Opened channel with id 0x%x \n", channel.ID())
	n.ch = channel

	return nil
}

func (n *Node) HandleProposal(_proposal client.ChannelProposal, responder *client.ProposalResponder) {
	// Check that we got a ledger channel proposal.
	proposal, ok := _proposal.(*client.LedgerChannelProposal)
	if !ok {
		fmt.Println("Received a proposal that was not for a ledger channel.")
		return
	}

	// Print the proposers address (his index is always 0).
	fmt.Printf("Received channel proposal from 0x%x\n", proposal.Peers[0])
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	// Create a channel accept message and send it.
	accept := proposal.Accept(n.account.Address(), client.WithRandomNonce())
	channel, err := responder.Accept(ctx, accept)
	if err != nil {
		fmt.Println("Accepting channel: %w\n", err)
	} else {
		fmt.Printf("Accepted channel with id 0x%x\n", channel.ID())
	}
}

func (n *Node) HandleNewChannel(ch *client.Channel) {
	fmt.Printf("%v HandleNewChannel with id 0x%x\n", n.role, ch.ID())
	n.ch = ch

	// Start the on-chain watcher.
	go func() {
		err := ch.Watch(n)
		fmt.Println("Watcher returned with: ", err)
	}()
}

func (n *Node) UpdateChannel() error {
	// ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	// Use UpdateBy to conveniently update the channels state.
	return n.ch.UpdateBy(ctx, func(state *channel.State) error {
		// Shift 1 ETH from bob to alice.
		amount := ethToWei(1)
		state.Balances[0][RoleClient].Sub(state.Balances[0][RoleClient], amount)
		state.Balances[0][RoleBroker].Add(state.Balances[0][RoleBroker], amount)
		// Finalize the channel, this will be important in the next step.
		state.IsFinal = true
		return nil
	})
}

func (n *Node) HandleUpdate(update client.ChannelUpdate, responder *client.UpdateResponder) {
	fmt.Printf("%v HandleUpdate Bals=%s\n", n.role, formatBalances(update.State.Balances))
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := responder.Accept(ctx); err != nil {
		fmt.Printf("Could not accept update: %v\n", err)
	}
}

func formatBalances(bals channel.Balances) string {
	return fmt.Sprintf("[%v, %v]", bals[0][RoleBroker], bals[0][RoleClient])
}

func (n *Node) CloseChannel() error {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	if err := n.ch.Register(ctx); err != nil {
		return fmt.Errorf("registering channel: %w", err)
	}
	if err := n.ch.Settle(ctx, false); err != nil {
		return fmt.Errorf("settling channel: %w", err)
	}
	// .Close() closes the channel object and has nothing to do with the
	// go-perun channel protocol.
	if err := n.ch.Close(); err != nil {
		return fmt.Errorf("closing channel object: %w", err)
	}
	close(n.done)
	return nil
}

func (n *Node) HandleAdjudicatorEvent(e channel.AdjudicatorEvent) {
	fmt.Printf("HandleAdjudicatorEvent called id=0x%x\n", e.ID())
	// Alice reacts to a channel closing by doing the same as Bob.
	if _, ok := e.(*channel.ConcludedEvent); ok && n.role == RoleBroker {
		n.CloseChannel()
	}
}

func ethToWei(eth float64) *big.Int {
	//1 Ether = 10^18 Wei
	var ethPerWei = new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil)
	wei, _ := new(big.Float).Mul(big.NewFloat(eth), new(big.Float).SetInt(ethPerWei)).Int(new(big.Int))
	return wei
}
