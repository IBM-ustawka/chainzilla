package chainzilla

import (
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/core/types"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
	perunhd "perun.network/go-perun/backend/ethereum/wallet/hd"
)

// setupWallet sets up perun's wallet back-end.
func setupWallet(role NodeRole, mnemonic string) (*perunhd.Account, *perunhd.Wallet, error) {
	rootWallet, err := hdwallet.NewFromMnemonic(mnemonic)
	if err != nil {
		return nil, nil, fmt.Errorf("creating hd wallet: %w", err)
	}

	// Broker has account index 0 and client 1.
	wallet, err := perunhd.NewWallet(rootWallet, "m/44'/60'/0'/0/0", uint(role))
	if err != nil {
		return nil, nil, fmt.Errorf("deriving path: %w", err)
	}

	// Derive the first account from the wallet.
	acc, err := wallet.NewAccount()
	if err != nil {
		return nil, nil, fmt.Errorf("deriving hd account: %w", err)
	}

	return acc, wallet, nil
}

func createTransactor(wallet *perunhd.Wallet) *perunhd.Transactor {
	// 1337 is the default chain id for ganache-cli.
	signer := types.NewEIP155Signer(big.NewInt(1337))
	return perunhd.NewTransactor(wallet.Wallet(), signer)
}
