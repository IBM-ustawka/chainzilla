package chainzilla

import (
	"context"
	"time"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"

	ethchannel "perun.network/go-perun/backend/ethereum/channel"
)

// connectToChain Connects to the chain and retrieves an ethereum contract back-end.
func connectToChain(transactor ethchannel.Transactor, chainURL string) (client *ethclient.Client, cb ethchannel.ContractBackend, err error) {
	client, err = ethclient.Dial(chainURL)
	if err != nil {
		return
	}

	return client, ethchannel.NewContractBackend(client, transactor), nil
}

func deployContracts(cb ethchannel.ContractBackend, acc accounts.Account) (adj, ah common.Address, err error) {
	// The context timeout must be atleast twice the blocktime.
	ctx, cancel := context.WithTimeout(context.Background(), 31*time.Second)
	defer cancel()

	adj, err = ethchannel.DeployAdjudicator(ctx, cb, acc)
	if err != nil {
		return
	}

	ah, err = ethchannel.DeployETHAssetholder(ctx, cb, adj, acc)
	return
}

func validateContracts(cb ethchannel.ContractBackend, adj, ah common.Address) error {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	// Assetholder validation includes Adjudicator validation.
	return ethchannel.ValidateAssetHolderETH(ctx, cb, ah, adj)
}

// setupContracts deploys or validates contracts based on given node role.
func setupContracts(role NodeRole, contractBackend ethchannel.ContractBackend,
	account accounts.Account, adjudicatorHex, assetholderHex string) (adjudicator *ethchannel.Adjudicator, assetholder common.Address, err error) {
	var adjudicatorAddr common.Address
	// Alice will deploy the contracts and Bob validate them.
	if role == RoleBroker {
		adjudicatorAddr, assetholder, err = deployContracts(contractBackend, account)
		// fmt.Println("Deployed contracts")
	} else {
		// Assume default addresses for Adjudicator and Assetholder.
		// adjudicatorAddr = common.HexToAddress("0x079557d7549d7D44F4b00b51d2C532674129ed51")
		// assetholder = common.HexToAddress("0x923439be515b6A928cB9650d70000a9044e49E85")
		adjudicatorAddr = common.HexToAddress(adjudicatorHex)
		assetholder = common.HexToAddress(assetholderHex)
		err = validateContracts(contractBackend, adjudicatorAddr, assetholder)
	}
	// fmt.Printf(" Adjudicator at %v\n AssetHolder at %v\n", adjudicatorAddr, assetholder)
	adjudicator = ethchannel.NewAdjudicator(contractBackend, adjudicatorAddr, account.Address, account)
	return
}
